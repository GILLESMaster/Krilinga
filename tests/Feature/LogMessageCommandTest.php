<?php

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Log\Events\MessageLogged;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class LogMessageCommandTest extends TestCase
{
    use RefreshDatabase;

    protected function getEnvironmentSetUp($app)
    {
        $app->configure('discordlogger');
    }

    public function testLogMessageCommand()
    {
        // Arrange
        Event::fake();
        $logMessage = 'Krilinga Pest test!';

        // Act
        $this->artisan('log:message', ['message' => $logMessage]);

        // Assert
        Event::assertDispatched(MessageLogged::class, function ($event) use ($logMessage) {
            return $event->message === $logMessage;
        });
    }
}
