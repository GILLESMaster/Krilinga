FROM php:8.2-fpm

 #Copy composer.lock and composer.json
#COPY  ./composer.json /var/www/

# Set working directory
WORKDIR /var/www

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl



# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*


# Install PHP extensions

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions mbstring pdo_mysql zip exif pcntl gd



# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#Installing node 20.x
RUN curl -sL https://deb.nodesource.com/setup_20.x| bash -
RUN apt-get install -y nodejs


# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

RUN chown -R www:www /var/www
# Copy existing application directory contents
COPY . /var/www

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

# Change current user to www
USER www

RUN composer install --no-interaction
RUN npm ci && npm run build
#RUN php artisan migrate --force

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]

# Set environment variables
ENV APP_NAME=Krilinga \
    APP_ENV=local \
    APP_KEY="" \
    APP_DEBUG=true \
    APP_URL=http://127.0.0.1 \
    ASSET_URL=${APP_URL} \
    LOG_CHANNEL=stack \
    LOG_DEPRECATIONS_CHANNEL=stack \
    LOG_LEVEL=debug \
    LOG_DISCORD_WEBHOOK_URL="" \
    AVATAR_URL="" \
    DB_CONNECTION=mysql \
    DB_HOST=common_mariadb \
    DB_PORT=3306 \
    DB_DATABASE=laravel \
    DB_USERNAME=root \
    DB_PASSWORD=Test \
    BROADCAST_DRIVER=log \
    CACHE_DRIVER=file \
    FILESYSTEM_DISK=local \
    QUEUE_CONNECTION=sync \
    SESSION_DRIVER=database \
    SESSION_LIFETIME=120 \
    CLOCKWORK_ENABLE=true \
    CLOCKWORK_COLLECT_DATA_ALWAYS=true \
    CLOCKWORK_CACHE_ENABLED=true \
    CLOCKWORK_DATABASE_ENABLED=true \
    CLOCKWORK_EVENTS_ENABLED=true \
    CLOCKWORK_LOG_ENABLED=true \
    CLOCKWORK_NOTIFICATIONS_ENABLED=true \
    CLOCKWORK_QUEUE_ENABLED=true \
    CLOCKWORK_REDIS_ENABLED=true \
    CLOCKWORK_ROUTES_ENABLED=true \
    CLOCKWORK_VIEWS_ENABLED=true \
    CLOCKWORK_ARTISAN_COLLECT=true \
    CLOCKWORK_WEB=false \
    CLOCKWORK_TOOLBAR=false \
    CLOCKWORK_AUTHENTICATION=false \
    CLOCKWORK_AUTHENTICATION_PASSWORD="" \
    CLOCKWORK_STORAGE=${DB_CONNECTION} \
    CLOCKWORK_STORAGE_SQL_DATABASE=${DB_DATABASE} \
    CLOCKWORK_STORAGE_SQL_TABLE=clockwork \
    GITHUB_CLIENT_ID="" \
    GITHUB_CLIENT_SECRET="" \
    GITHUB_REDIRECT=http://localhost:8000/oauth/github/callback \
    GOOGLE_CLIENT_ID="" \
    GOOGLE_CLIENT_SECRET="" \
    GOOGLE_REDIRECT=http://localhost:8000/oauth/google/callback