#!/bin/bash

# Remote host details
remote_host="host"
remote_user="user"
remote_port="port"

# Local destination directory
local_directory="~/Documents"

# Associative array of log files and their corresponding directories
declare -A log_files=(
    ["deployment.log"]="path/to/project"
    ["laravel.log"]="path/to/project/storage/logs"
    ["access.log"]="path/to/nginx"
    ["error.log"]="path/to/nginx"
    ["php{version}-fpm.log"]="path/to/logs"
)

# Check if sshpass is installed
if ! command -v sshpass >/dev/null 2>&1; then
    echo "sshpass is not installed."

    # Check if apt is available
    if command -v apt >/dev/null 2>&1; then
        echo "Installing sshpass using apt..."
        sudo apt update
        sudo apt install -y sshpass
    # Check if pacman is available
    elif command -v pacman >/dev/null 2>&1; then
        echo "Installing sshpass using pacman..."
        sudo pacman -Syu --noconfirm sshpass
    else
        echo "Failed to install sshpass. Please install it manually and run the script again."
        exit 1
    fi
fi

# Prompt for the remote password
read -s -p "Enter the remote password: " remote_password
echo

# Iterate over log files and download them
for log_file in "${!log_files[@]}"; do
    remote_directory="${log_files[$log_file]}"
    remote_file="${remote_user}@${remote_host}:${remote_directory}/${log_file}"
    local_file="${local_directory}/${log_file}"

    # Create the local directory if it doesn't exist
    mkdir -p "$(dirname "${local_file}")"

    # Download the file using sshpass with scp
    sshpass -p "${remote_password}" scp -q -P "${remote_port}" "${remote_file}" "${local_file}"

    if [ $? -eq 0 ]; then
        echo "Downloaded ${log_file} successfully."
    else
        echo "Failed to download ${log_file}."
    fi
done

# Print local_directory
echo
echo Downloaded to file directory:
echo $local_directory
echo