#!/bin/bash

set -e  # Exit immediately if any command fails

PROJECT_PATH="path/to/project"

# Change directory to the project path
cd "$PROJECT_PATH"

# Reset to the latest commit on the master branch
if ! git reset --hard origin/master; then
  echo "Git reset failed. Exiting..."
  exit 1
fi

# Attempt to pull the latest changes of the project
if ! git pull; then
  echo "Pull without rebase failed. Retrying with --rebase..."
  if ! git pull --rebase; then
    echo "Pull with rebase failed. Exiting..."
    exit 1
  fi
fi

# Sleep for a few seconds to allow for file system changes to settle
sleep 2

# Set environment variables
export COMPOSER_ALLOW_SUPERUSER=1
export COMPOSER_NO_INTERACTION=1

# Install project dependencies using Composer
if ! composer install; then
  echo "Composer installation failed. Exiting..."
  exit 1
fi

# Sleep for a few seconds to allow for Composer installation to complete
sleep 2

# Restart nginx service
if ! systemctl restart nginx.service; then
  echo "Failed to restart nginx service. Exiting..."
  exit 1
fi

# Restart php-fpm service
if ! systemctl restart php{version}-fpm.service; then
  echo "Failed to restart php-fpm service. Exiting..."
  exit 1
fi

# Sleep for a few seconds to allow for the services to have restarted
sleep 2

# Check if any of the services failed to start
if ! systemctl is-active --quiet nginx.service || ! systemctl is-active --quiet php{version}-fpm.service; then
  echo "One or more services failed to start. Rebooting the system..."
  sleep 5
  reboot
fi
