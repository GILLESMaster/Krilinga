<h1 align="center">
  <span style="font-size: 72px">Krilinga</span>
  <br>
  <br>
  <img src="https://raw.githubusercontent.com/GILLESMaster/Krilinga/master/.github/favicon.svg" alt="Logo" width="100" height="100">
</h1>

<p align="center">
  <a href="https://github.com/GILLESMaster/Krilinga/actions/workflows/tests.yml">
    <img src="https://github.com/GILLESMaster/Krilinga/actions/workflows/tests.yml/badge.svg?style=flat" alt="tests">
  </a>
  <a href="https://github.com/GILLESMaster/Krilinga/commits/master">
    <img src="https://img.shields.io/github/last-commit/GILLESMaster/Krilinga.svg" alt="GitHub last commit">
  </a>
  <a href="https://github.com/GILLESMaster/Krilinga/blob/master/LICENSE">
    <img src="https://img.shields.io/badge/license-MIT-blue.svg" alt="GitHub license">
  </a>
  <a href="https://styleci.io/repos/647890269?branch=master">
    <img src="https://styleci.io/repos/647890269/shield?style=flat&branch=master" alt="StyleCI">
  </a>
</p>

Krilinga is a web application project built on the Laravel framework with a Vite and Vue powered frontend. It provides an easy to learn Laravel/Vite/Vue and includes fun games.

## Features

- Laravel framework for server-side functionality.
- Vite/Vue for fast and efficient frontend development.
- Easy-to-use and well-documented codebase.

## Installation

Before proceeding with the installation, please ensure that you have the following prerequisites installed:

- PHP ^8.2.x
- NodeJS ^v20.2.x

1. Clone the repository:

   ```bash
   git clone https://github.com/GILLESMaster/Gustav-PHP.git
   ```

2. Install dependencies:

   ```bash
   composer install
   npm install
   ```

3. Configure the environment variables:

   ```bash
   cp .env.example .env
   php artisan key:generate
   ```

4. Run database migrations and seeders:

   ```bash
   php artisan migrate --seed
   ```

5. Build frontend assets:

   ```bash
   npm run build
   ```

6. Start the development server:

   ```bash
   php artisan serve
   ```

7. run tests (optional):

   ```bash
   vendor/bin/pest
   ```

## Usage

- Make sure to change the environment variables in the [DotEnv](https://github.com/GILLESMaster/Krilinga/blob/master/.env.example) file!
- Also make sure you have the desired OAuth providers!
- Create cache with `php artisan create:caches`.
- Remove cache with `php artisan remove:cached`.
- Access the application in your browser at `http://localhost:8000`.
- Create a new account.
- Explore the application and its features.

## Docker (experimental!)

- Make sure to change the environment variables in the [docker-composer](https://github.com/GILLESMaster/Krilinga/blob/master/docker-composer.yml) file!
- Have Docker and Docker-Composer installed.
- Create docker containers with `docker-compose up -d`.
- Access the application in your browser at `http://127.0.0.1`.
- Make sure to test the application!

## Kubernetes (experimental!)

- Under construction...

## Production

- Make sure to change the environment variables in the [DotEnv](https://github.com/GILLESMaster/Krilinga/blob/master/.env.example) file!
- Make sure that the local enviorment works before going into production!
- Create cache with `php artisan create:caches`.
- Use a web server (for example NGINX with [nginx.conf](https://github.com/GILLESMaster/Krilinga/blob/master/nginx.example.conf).)
- Now that the server is running and set up you can also create a seperate DB for production.
- You must now setup certbot for a ssl certificate to get support for the https protocol (it is enforced by default)!
- Access the application in your browser at `https://yourip`.
- Make sure to test the application!

## Production (Docker)

- Under construction...

## Production (Kubernetes)

- Under construction...

## Contributing

Contributions are welcome! If you encounter any bugs or have suggestions for improvements, please [open an issue](https://github.com/GILLESMaster/Krilinga/issues) or submit a pull request.

## License

This project is licensed under the [MIT License](https://github.com/GILLESMaster/Krilinga/blob/master/LICENSE).

## Acknowledgements

This project utilizes the following open-source libraries and frameworks:

- [![Laravel](https://img.shields.io/badge/-Laravel-FF2D20?logo=laravel&logoColor=white)](https://laravel.com/)
- [![Vite](https://img.shields.io/badge/-Vite-646CFF?logo=vite&logoColor=white)](https://vitejs.dev/)
- [![Vue](https://img.shields.io/badge/-Vue-4FC08D?logo=vue.js&logoColor=white)](https://vuejs.org)

This project also integrates the following technologies:

- [![Docker](https://img.shields.io/badge/-Docker-2496ED.svg?logo=docker&logoColor=white)](https://www.docker.com/)
- [![Kubernetes](https://img.shields.io/badge/-Kubernetes-326CE5.svg?logo=kubernetes&logoColor=white)](https://kubernetes.io/)
- [![Nginx](https://img.shields.io/badge/-Nginx-006400.svg?logo=nginx&logoColor=white)](https://nginx.org/)
- [![PHP](https://img.shields.io/badge/-PHP-777BB4?logo=php&logoColor=white)](https://www.php.net/)
- [![Node.js](https://img.shields.io/badge/-Node.js-339933?logo=node.js&logoColor=white)](https://nodejs.org/)