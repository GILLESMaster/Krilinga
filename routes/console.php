<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('log:message {message}', function ($message) {
    Log::debug($message);
})->purpose('Log a message to the debug log');

Artisan::command('create:caches', function () {
    $this->call('route:cache');
    $this->call('event:cache');
    $this->call('view:cache');
})->purpose('Create caches for routes, events, and views');

Artisan::command('remove:cached', function () {
    $this->call('route:clear');
    $this->call('event:clear');
    $this->call('view:clear');
    $this->call('config:clear');
    $this->call('cache:clear');
})->purpose('Remove cached files for routes, events, views, config, and cache');
