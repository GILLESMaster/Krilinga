##                                                                                                         ##
#   Welcome to the Krilinga DotEnv.                                                                         #
#                                                                                                           #
#   More info:                                                                                              #
#                                                                                                           #
#   https://laravel.com/docs/10.x/configuration#environment-configuration                                   #
#                                                                                                           #
#   _    _         _  _  _                                                                                  #
#  | |  / )       (_)| |(_)                                                                                 #
#  | | / /   ____  _ | | _  ____    ____   ____                                                             #
#  | |< <   / ___)| || || ||  _ \  / _  | / _  |                                                            #
#  | | \ \ | |    | || || || | | |( ( | |( ( | |                                                            #
#  |_|  \_)|_|    |_||_||_||_| |_| \_|| | \_||_|                                                            #
#                                 (_____|                                                                   #
#                                                                                                           #
#   https://github.com/GILLESMaster/Krilinga                                                                #
#                                                                                                           #
#       --Gustavo Schip                                                                                     #
#                                                                                                           #
#   KEY VARIABLES!                                                                                          #
#                                                                                                           #
#   MUST CONFIGURE!                                                                                         #
##                                                                                                         ##

APP_NAME=Krilinga
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://127.0.0.1
ASSET_URL="${APP_URL}"

##                                                                                                         ##
#   Common log variables.                                                                                   #
##                                                                                                         ##

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=stack
LOG_LEVEL=debug

##                                                                                                         ##
#   Webhook based logs.                                                                                     #
##                                                                                                         ##

LOG_DISCORD_WEBHOOK_URL=
AVATAR_URL=

##                                                                                                         ##
#   Database variables:                                                                                     #
#                                                                                                           #
#   https://laravel.com/docs/10.x/database                                                                  #
##                                                                                                         ##

DB_CONNECTION=mysql
DB_HOST=common_mariadb
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=Test

##                                                                                                         ##
#   Common variables.                                                                                       #
##                                                                                                         ##

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DISK=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=database
SESSION_LIFETIME=120

##                                                                                                         ##
#   Clockwork's activity variables.                                                                         #
##                                                                                                         ##

CLOCKWORK_ENABLE=true
CLOCKWORK_COLLECT_DATA_ALWAYS=true

##                                                                                                         ##
#   Clockwork's features:                                                                                   #
#                                                                                                           #
#   https://underground.works/clockwork/#docs-collected-data                                                #
##                                                                                                         ##

CLOCKWORK_CACHE_ENABLED=true
CLOCKWORK_DATABASE_ENABLED=true
CLOCKWORK_EVENTS_ENABLED=true
CLOCKWORK_LOG_ENABLED=true
CLOCKWORK_NOTIFICATIONS_ENABLED=true
CLOCKWORK_QUEUE_ENABLED=true
CLOCKWORK_REDIS_ENABLED=true
CLOCKWORK_ROUTES_ENABLED=true
CLOCKWORK_VIEWS_ENABLED=true

##                                                                                                         ##
#   Clockwork's command logging feature.                                                                    #
##                                                                                                         ##

CLOCKWORK_ARTISAN_COLLECT=true

##                                                                                                         ##
#   Clockwork's extra features:                                                                             #
#                                                                                                           #
#   https://underground.works/clockwork/#docs-viewing-data                                                  #
##                                                                                                         ##

CLOCKWORK_WEB=false
CLOCKWORK_TOOLBAR=false

##                                                                                                         ##
#   Clockwork's Authentication:                                                                             #
#                                                                                                           #
#   Example:                                                                                                #
#                                                                                                           #
#       CLOCKWORK_AUTHENTICATION=true                                                                       #
#       CLOCKWORK_AUTHENTICATION_PASSWORD="ChAnGeMe"                                                        #
##                                                                                                         ##

CLOCKWORK_AUTHENTICATION=false
CLOCKWORK_AUTHENTICATION_PASSWORD=""

##                                                                                                         ##
#   Clockwork's database variables.                                                                         #
##                                                                                                         ##

CLOCKWORK_STORAGE="${DB_CONNECTION}"
CLOCKWORK_STORAGE_SQL_DATABASE="${DB_DATABASE}"
CLOCKWORK_STORAGE_SQL_TABLE=clockwork

##                                                                                                         ##
#   Github OAuth:                                                                                           #
#                                                                                                           #
#   https://github.com/settings/developers                                                                  #
#   https://docs.socialstream.dev/getting-started/routing                                                   #
##                                                                                                         ##

GITHUB_CLIENT_ID=
GITHUB_CLIENT_SECRET=
GITHUB_REDIRECT='http://localhost:8000/oauth/github/callback'

##                                                                                                         ##
#   Google OAuth:                                                                                           #
#                                                                                                           #
#   https://console.cloud.google.com/apis/credentials                                                       #
#   https://docs.socialstream.dev/getting-started/routing                                                   #
##                                                                                                         ##

GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=
GOOGLE_REDIRECT='http://localhost:8000/oauth/google/callback'

##                                                                                                         ##
#                                                    END                                                    #
##                                                                                                         ##