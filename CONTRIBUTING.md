## Contributing

Contributions are welcome! If you encounter any bugs or have suggestions for improvements, please [open an issue](https://github.com/GILLESMaster/Krilinga/issues) or submit a pull request.
